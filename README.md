# README #
This payment driver is designed to receive payment orders for CMS Prestashop

=== Wallet One Checkout ===
Contributors: h-elena
Version: 3.3.1
Requires at least: 1.6.1.5
Tested up to: 1.7.0.0
Stable tag: 1.7.0.0
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One Checkout module is a payment system for CMS Prestashop.

== Description ==
If you have an online store, then you need a module payments for orders made.

== Installation ==
1. Register on the site https://www.walletone.com/merchant/client/#/signup
2. Download the module on the repository .
3. Instructions for configuring the module is in the file read.pdf.

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==

= 1.0.1 =
* Fix - Fix creating order and clear cart

= 2.0.0 =
* Fix - Add universals classes and new settings

= 2.0.1 =
* Fix - fixed bug with checkin signature in codding string windows-1251

= 2.1.0 =
* Added support for new version 1.7.0.0

= 3.0.0 =
* Fix - fixed bug with anser from calback payment system

= 3.1.0 =
* Change expired date for ingoice(30 days), fixed bug with currency change in settings

= 3.1.1 =
* Fixed bug with "culture_id" in settings

= 3.2.0 =
* Add - add new payment types (Viber and Apple Pay)

= 3.2.1 =
* Fix - added check of the method of sending email

= 3.3.0 =
* Add - added 54-fz support

= 3.3.1 =
* Fixed - json encode for orderItems

== Frequently Asked Questions ==
No recent asked questions
